# virtù - openapi-dev-tool



### Installing 

* Install npm 
* npm i -g @lyra-network/openapi-dev-tool
* Usage: _openapi-dev-tool help_


### Config 

* Create a config file according to https://github.com/lyra/openapi-dev-tool#configuration-file 

### Serve
```bash
openapi-dev-tool serve
```

